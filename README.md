# Description Field

## Introduction
The `Description Field` module provides a simple form display field that you can put display only text into for showing on forms. Useful when you want to keep the text within the form and you don't want to have the overhead of creating a block or creating specific code to achieve the same result.

## Requirements
No special requirements.

## Installation
Install as you would normally install a contributed Drupal module. See: https://www.drupal.org/documentation/install/modules-themes/modules-8

Once enabled, a `Description Field` field type will be available from the `Add field` form in the `Add a new field` select field.

## Maintainers
Current Maintainers:
* Michael Welford - https://www.drupal.org/u/mikejw

This project has been sponsored by:
* The University of Adelaide - https://www.drupal.org/university-of-adelaide
  Visit: http://www.adelaide.edu.au
